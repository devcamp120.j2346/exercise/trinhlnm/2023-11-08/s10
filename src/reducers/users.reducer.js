import {
    USERS_FETCH_ERROR,
    USERS_FETCH_PENDING,
    USERS_FETCH_SUCCESS,
    USERS_PAGE_CHANGE
} from "../constants/users.constant";

const initialState = {
    users: [],
    pending: false,
    error: null,
    limit: 3,
    noPage: 0,
    currentPage: 1
}

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case USERS_FETCH_PENDING:
            state.pending = true;
            break;
        case USERS_FETCH_SUCCESS:
            state.pending = false;
            state.noPage = Math.ceil(action.totalUser / state.limit);
            state.users = action.data;
            break;
        case USERS_FETCH_ERROR:
            break;
        case USERS_PAGE_CHANGE:
            state.currentPage = action.page;
            break;
        default:
            break;
    }

    return { ...state };
}

export default userReducer;
