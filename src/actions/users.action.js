import {
    USERS_FETCH_ERROR,
    USERS_FETCH_PENDING,
    USERS_FETCH_SUCCESS,
    USERS_PAGE_CHANGE
} from "../constants/users.constant";

export const fetchUsers = (page, limit) => {
    return async (dispatch) => {
        try {
            var requestOptions = {
                method: 'GET',
                redirect: 'follow'
            };
    
            await dispatch({
                type: USERS_FETCH_PENDING
            })

            const responseTotalUser = await fetch("https://jsonplaceholder.typicode.com/users", requestOptions);

            const dataTotalUser = await responseTotalUser.json();

            const params = new URLSearchParams({
                _start: (page-1) * limit, 
                _limit: limit
            });

            const response = await fetch("https://jsonplaceholder.typicode.com/users?" + params.toString(), requestOptions);

            const data = await response.json();

            
            return dispatch({
                type: USERS_FETCH_SUCCESS,
                totalUser: dataTotalUser.length,
                data: data
            })
        } catch (error) {
            return dispatch({
                type: USERS_FETCH_ERROR,
                error: error
            })
        }   
    }
}

export const pageChangePagination = (page) => {
    return {
        type: USERS_PAGE_CHANGE,
        page: page
    }
}
