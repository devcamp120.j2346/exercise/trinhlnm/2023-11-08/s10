import { CircularProgress, Container, Grid, Pagination, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@mui/material"
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { fetchUsers, pageChangePagination } from "../actions/users.action";

const Users = () => {
    const dispatch = useDispatch();

    const { users, pending, noPage, currentPage, limit } = useSelector((reduxData) => reduxData.userReducer);

    useEffect(() => {
        // Gọi API để lấy dữ liệu
        dispatch(fetchUsers(currentPage, limit));
    }, [currentPage]);

    const onChangePagination = (event, value) => {
        dispatch(pageChangePagination(value));
    }

    return (
        <Container>
            <Grid container mt={5}>
                {
                    pending ?
                        <Grid item lg={12} md={12} sm={12} xs={12} textAlign="center">
                            <CircularProgress />
                        </Grid>
                        :
                        <>
                            <Grid item lg={12} md={12} sm={12} xs={12}>
                                <TableContainer component={Paper}>
                                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                                        <TableHead>
                                            <TableRow>
                                                <TableCell>ID</TableCell>
                                                <TableCell>Name</TableCell>
                                                <TableCell>Username</TableCell>
                                                <TableCell>Email</TableCell>
                                                <TableCell>Phone</TableCell>
                                                <TableCell>Website</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {
                                                users.map((element, index) => {
                                                    return (
                                                        <TableRow key={index}>
                                                            <TableCell>{element.id}</TableCell>
                                                            <TableCell>{element.name}</TableCell>
                                                            <TableCell>{element.username}</TableCell>
                                                            <TableCell>{element.email}</TableCell>
                                                            <TableCell>{element.phone}</TableCell>
                                                            <TableCell>{element.website}</TableCell>
                                                        </TableRow>
                                                    )
                                                })
                                            }
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </Grid>

                            <Grid item lg={12} md={12} sm={12} xs={12} mt={5}>
                                <Pagination count={noPage} page={currentPage} onChange={onChangePagination} />
                            </Grid>

                        </>
                }
            </Grid>
        </Container>
    )
}

export default Users;
